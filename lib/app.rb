require 'sinatra'
require 'erb'

personal_info = { Name: ['Felipe Barbosa Teixeira'],
                  Hobbies: ['"Você não tem." - minha esposa.',
                            'Ler', 'Música', 'Filmes'],
                  Trivia: ['A linguagem com a qual passei mais tempo programando é LISP'],
                  Values: %w[Honestidade Curiosidade Disciplina],
                  Home: ['Uma bagunça, mas melhorando', 'Moro em Vitória, ES'],
                  Family: ['Esposa, casado há 4 anos', 'Filho (8 meses)'],
                  Goals: ['Nova carreira', 'Mais tempo pessoal', 'Não falir em 2022'],
                  Education: ['Graduação & Mestrado em Engenharia Civil'],
                  Work: ['Workaholic em tratamento',
                         'Atuação na área de engenharia civil: calculista estrutural'] }

merge_request_links = { 'game-log-parser': { 1 => ['print-log-head', 'https://gitlab.com/felipe.teixeira2/game-log-parser/-/merge_requests/1'],
                                             2 => ['count-lines', 'https://gitlab.com/felipe.teixeira2/game-log-parser/-/merge_requests/2'],
                                             3 => ['list-players', 'https://gitlab.com/felipe.teixeira2/game-log-parser/-/merge_requests/3'],
                                             4 => ['count-kills', 'https://gitlab.com/felipe.teixeira2/game-log-parser/-/merge_requests/4'],
                                             5 => ['rubocop-simplecov', 'https://gitlab.com/felipe.teixeira2/game-log-parser/-/merge_requests/5'] },
                        'sinatra-webapp': { 1 => ['hello-world', 'https://gitlab.com/felipe.teixeira2/sinatra-webapp/-/merge_requests/1'],
                                            2 => ['rspec-lint', 'https://gitlab.com/felipe.teixeira2/sinatra-webapp/-/merge_requests/2'],
                                            3 => ['personal-info', 'https://gitlab.com/felipe.teixeira2/sinatra-webapp/-/merge_requests/3'],
                                            4 => ['MR-links', 'https://gitlab.com/felipe.teixeira2/sinatra-webapp/-/merge_requests/4'] } }

get '/' do
  erb :index, locals: { info: personal_info }
end

get '/links' do
  erb :links, locals: { mr_links: merge_request_links }
end
