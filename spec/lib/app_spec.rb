require 'app'
require 'rack/test'

# set :environment, :test

def app
  Sinatra::Application
end

describe 'Check server online' do
  include Rack::Test::Methods
  it 'should load home page' do
    get '/'
    expect(last_response.status).to eq(200)
  end

  it 'should load links page' do
    get '/links'
    expect(last_response.status).to eq(200)
  end

  it 'should not load fake page' do
    get '/nosuchpage'
    expect(last_response.status).to eq(404)
  end
end
